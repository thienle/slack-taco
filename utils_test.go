package p

import (
	"reflect"
	"testing"
)

func Test_getNumberEmoji(t *testing.T) {
	type args struct {
		number int
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{"One", struct{ number int }{number: 1}, []string{"one"}},
		{"Two", struct{ number int }{number: 2}, []string{"two"}},
		{"Three", struct{ number int }{number: 3}, []string{"three"}},
		{"Four", struct{ number int }{number: 4}, []string{"four"}},
		{"Five", struct{ number int }{number: 5}, []string{"five"}},
		{"Six", struct{ number int }{number: 6}, []string{"six"}},
		{"Seven", struct{ number int }{number: 7}, []string{"seven"}},
		{"Eight", struct{ number int }{number: 8}, []string{"eight"}},
		{"Nine", struct{ number int }{number: 9}, []string{"nine"}},
		{"Ten", struct{ number int }{number: 10}, []string{"keycap_ten"}},
		{"TwoNumber", struct{ number int }{number: 18}, []string{"one", "eight"}},
		{"ThreeNumber", struct{ number int }{number: 189}, []string{"one", "eight", "nine"}},
		{"DuplicateNumber", struct{ number int }{number: 181}, []string{"one", "eight", "one"}},
		{"Zero", struct{ number int }{number: 0}, nil},
		{"Negative", struct{ number int }{number: -3}, nil},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getNumberEmoji(tt.args.number); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getNumberEmoji() = %v, wantFrom %v", got, tt.want)
			}
		})
	}
}
