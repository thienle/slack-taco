// Code generated by "stringer -type Duration duration.go"; DO NOT EDIT.

package p

import "strconv"

func _() {
	// An "invalid array index" compiler error signifies that the constant values have changed.
	// Re-run the stringer command to generate them again.
	var x [1]struct{}
	_ = x[Day-0]
	_ = x[Week-1]
	_ = x[Sprint-2]
	_ = x[Month-3]
	_ = x[Year-4]
}

const _Duration_name = "DayWeekSprintMonthYear"

var _Duration_index = [...]uint8{0, 3, 7, 13, 18, 22}

func (i Duration) String() string {
	if i < 0 || i >= Duration(len(_Duration_index)-1) {
		return "Duration(" + strconv.FormatInt(int64(i), 10) + ")"
	}
	return _Duration_name[_Duration_index[i]:_Duration_index[i+1]]
}
