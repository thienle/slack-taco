package p

import (
	"github.com/nlopes/slack"
	"reflect"
	"strings"
	"testing"
)

func Test_determineDuration(t *testing.T) {
	type args struct {
		text string
	}
	tests := []struct {
		name string
		args args
		want Duration
	}{
		{"HappyPathDay", struct{ text string }{text: "chart day"}, Day},
		{"HappyPathWeek", struct{ text string }{text: "chart week day"}, Week},
		{"HappyPathSprint", struct{ text string }{text: "chart sprint week day"}, Sprint},
		{"HappyPathMonth", struct{ text string }{text: "chart month    \t"}, Month},
		{"HappyPathYear", struct{ text string }{text: "chart year"}, Year},
		{"MalformedArg", struct{ text string }{text: "chartyear"}, Day},
		{"EmptyArg", struct{ text string }{text: ""}, Day},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := determineDuration(tt.args.text); got != tt.want {
				t.Errorf("determineDuration() = %v, wantFrom %v", got, tt.want)
			}
		})
	}
}

func Test_findFirstUserIdIn(t *testing.T) {
	type args struct {
		text string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"HappyPath", struct{ text string }{text: "<@123456789>"}, "123456789"},
		{"TextHasTwoUserId", struct{ text string }{text: "<@123456789><@234567890>"}, "123456789"},
		{"EmptyText", struct{ text string }{text: ""}, ""},
		{"EmptyId", struct{ text string }{text: "<@>"}, ""},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := findFirstUserIdIn(tt.args.text); got != tt.want {
				t.Errorf("findFirstUserIdIn() = %v, wantFrom %v", got, tt.want)
			}
		})
	}
}

func Test_prepareRecord(t *testing.T) {
	type args struct {
		strTimestamp string
		giver        *slack.User
		receiver     *slack.User
		toGive       int
		message      string
	}
	giver := slack.User{Profile: slack.UserProfile{RealName: "GiverRealName"}, ID: "111"}
	receiver := slack.User{Profile: slack.UserProfile{RealName: "ReceiverRealName"}, ID: "222"}
	time := timeIn(location, toDate(strings.Split("1547921475.007300", ".")[0]))
	time2 := time.Format("01/02/2006 15:04:05")
	tests := []struct {
		name string
		args args
		want []interface{}
	}{
		{"CorrectFormat", struct {
			strTimestamp string
			giver        *slack.User
			receiver     *slack.User
			toGive       int
			message      string
		}{"1547921475.007300", &giver, &receiver, 5, "message"}, []interface{}{time, time2, "GiverRealName", "111", "ReceiverRealName", "222", 5, "message"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := prepareRecord(tt.args.strTimestamp, tt.args.giver, tt.args.receiver, tt.args.toGive, tt.args.message); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("prepareRecord() = %v, want %v", got, tt.want)
			}
		})
	}
}
