package p

type Emoji string

const (
	NotAllow Emoji = "x"
	Pray     Emoji = "pray"
	NoGood   Emoji = "no_good"
)
