// Package p contains an HTTP Cloud Function.
package p

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// location Location name Vietnam
const location = "Vietnam"

// mainEmoji name of the emoji to give
var mainEmoji = fmt.Sprintf(":%s:", os.Getenv("EMOJI_NAME"))

// mainEmojiPattern Compile Slack main emoji pattern first for better performance
var mainEmojiPattern = regexp.MustCompile(strings.Replace(fmt.Sprintf("(%s){1}", mainEmoji), "-", "\\-", -1))

// dayLimit Maximum number of emoji can be given everyday by each user
var dayLimit, _ = strconv.Atoi(os.Getenv("MAX_EVERYDAY"))

// sprintStart A start date of the sprint with layout dd MM yyyy
var sprintStart, _ = time.Parse("02 01 2006", os.Getenv("SPRINT_START_DATE"))

// sprintDuration Sprint Duration in day
var sprintDuration, _ = strconv.Atoi(os.Getenv("SPRINT_DURATION"))

//	verificationToken Slack verification token
var verificationToken = os.Getenv("VERIFICATION_TOKEN")

//	spreadsheetID if of the spreadsheet
var spreadsheetID = os.Getenv("SPREADSHEET_ID")

//	spreadsheetURL Shareable link to the spreadsheet
var spreadsheetURL = os.Getenv("SPREADSHEET_URL")

// Handle handle every requests
func Handle(w http.ResponseWriter, r *http.Request) {
	buffer := new(bytes.Buffer)
	_, err := buffer.ReadFrom(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Fatal("Error reading buffer from body.")
		return
	}
	log.Printf("Header: %v\n", r.Header)
	body := buffer.String()
	log.Printf("Body: %v\n", body)
	succeed := parseEvent(body, w)
	if !succeed {
		log.Printf("Unable to parse event. Error %v\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	log.Printf("Done")
	return
}
