package p

import "strings"

type Command string

const (
	Help  Command = "help"
	Chart         = "chart"
)

func DetermineCommand(text string) Command {
	if strings.HasPrefix(text, Chart) {
		return Chart
	} else {
		return Help
	}
}
