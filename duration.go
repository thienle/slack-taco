package p

import "strings"

// Declare a new type named Duration which will unify our enum values
// It has an underlying type of unsigned integer (uint).
type Duration int

// Declare typed constants each with type of Duration
const (
	Day Duration = iota
	Week
	Sprint
	Month
	Year
)

func DetermineDuration(str string) Duration {
	switch strings.ToLower(str) {
	case strings.ToLower(Day.String()):
		return Day
	case strings.ToLower(Week.String()):
		return Week
	case strings.ToLower(Sprint.String()):
		return Sprint
	case strings.ToLower(Month.String()):
		return Month
	case strings.ToLower(Year.String()):
		return Year
	default:
		return Day
	}
}
